package org.ws4d.mobile.authenticator.device;

import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.wscompactsecurity.authentication.AuthenticationService;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;

public class TheDevice extends DefaultDevice {

	public TheDevice() {
		this(null, null);
	}

	public TheDevice(OperationDataCallback cb, QNameSet additional) {
		super();
		
		this.setBasicPortTypes();
		
		if (additional != null) {
			this.setAdditionalPortTypes(additional);
		}
		
		this.addFriendlyName("en-US", "Android DPWS Device");
		
		this.addManufacturer("en-US", "ws4d.org");
		
		this.addModelName("en-US", "Rev. 01");
		
		// FIXME!! no hard coding! 
		this.setEndpointReference(new EndpointReference(new URI("urn:uuid:8c97de53-964a-4f0b-bb9d-8b4bdb415c22")));
		
		this.addService(new AuthenticationService(cb));
	}
	
	public TheDevice(OperationDataCallback cb) {
		this(cb, null);
	}
	
	public void setBasicPortTypes() {
		QNameSet portTypes = new QNameSet();
		portTypes.add(new QName(org.ws4d.mobile.authenticator.device.Constants.PortType, org.ws4d.mobile.authenticator.device.Constants.Namespace));
		portTypes.add(new QName(org.ws4d.wscompactsecurity.authentication.Constants.DevicePortTypeUIAuthenticator, org.ws4d.wscompactsecurity.authentication.Constants.Namespace));
		portTypes.add(new QName(org.ws4d.wscompactsecurity.authentication.Constants.DevicePortTypeAuthenticator, org.ws4d.wscompactsecurity.authentication.Constants.Namespace));
		this.setPortTypes(portTypes);
	}

	public void setAdditionalPortTypes(QNameSet addtionalPortTypes) {
		
		QNameSet completePortTypes = addtionalPortTypes;
		
		ReadOnlyIterator portTypes = (ReadOnlyIterator) this.getPortTypes();
		
		while (portTypes.hasNext()) {
			completePortTypes.add((QName) portTypes.next());
		}

		this.setPortTypes(completePortTypes);
	}
	
	public void resetPortTypes() {
		setBasicPortTypes();
	}
	
}
