package org.ws4d.wscompactsecurity.authentication.eccdh;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.wscompactsecurity.authentication.Constants;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;
import org.ws4d.wscompactsecurity.authentication.eccdh.types.RequestSecurityTokenResponse;
import org.ws4d.wscompactsecurity.authentication.eccdh.types.RequestSecurityTokenResponseCollection;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;
import org.ws4d.wscompactsecurity.authentication.engine.AuthenticationEngine;

import android.util.Log;

public class ECC_DH2 extends Operation {

	OperationDataCallback cb = null;
	
	public ECC_DH2() {
		this(null);
	}
	
	public ECC_DH2(OperationDataCallback cb) {
		super("ECC_DH2", new QName(Constants.AuthenticationServiceECCDHPortType, Constants.Namespace));
		this.cb = cb;
		
		Element in = new Element(new QName("RequestSecurityTokenResponse", Constants.Namespace_WS_Trust), new RequestSecurityTokenResponse());
		Element out = new Element(new QName("RequestSecurityTokenResponseCollection", Constants.Namespace_WS_Trust), new RequestSecurityTokenResponseCollection());
		
		Fault f1 = new Fault("SimpleError");
		f1.setElement(new Element(new QName("message", "http://www.ws4d.org"), SchemaUtil.TYPE_STRING));
		this.addFault(f1);
		
		this.setInput(in);
		this.setOutput(out);
		
	}
	
	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {
		
		String origin = ParameterValueManagement.getString(parameterValue, "OnBehalfOf");
		AuthenticationEngine engine = AuthenticationEngine.getInstance();
		ActiveAuthentication authentication = engine.getActiveAuthenticationByOrigin(origin);
		
		ParameterValue result = createOutputValue();
		
		authentication.setOriginNonce(ParameterValueManagement.getString(parameterValue, "RequestedSecurityToken/auth-ecc-dh-nonce"));
		authentication.setOriginPublic(ParameterValueManagement.getString(parameterValue, "RequestedSecurityToken/auth-ecc-dh-public-key"));
		authentication.setOldOriginCMAC(ParameterValueManagement.getString(parameterValue, "RequestedSecurityToken/auth-ecc-dh-cmac"));
		
		if (engine.checkOriginCMac(authentication)) {
			Log.d("ws4d", "Origin's CMAC passed test. Will calculate new one.");
			authentication.setNewOriginCMAC(Base64Util.encodeBytes(engine.calculateNewOriginCMac(authentication)));
		} else {
			Log.w("ws4d", "Origin's CMAC failed test. Will simply forward.");
			authentication.setNewOriginCMAC(authentication.getOldOriginCMAC());
		}
		
		if (cb != null) {
			authentication = cb.authenticationCallback2(authentication);
		}
		
		if (authentication.getErrorOccured()) {
			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", authentication.getErrorMessage());
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}
		
		if (engine.checkTargetCMac(authentication)) {
			Log.d("ws4d", "Target's CMAC passed test. Will calculate new one.");
			authentication.setNewTargetCMAC(Base64Util.encodeBytes(engine.calculateNewTargetCMac(authentication)));
		} else {
			Log.w("ws4d", "Target's CMAC failed test. Will simply forward.");
			authentication.setNewTargetCMAC(authentication.getOldTargetCMAC());
		}
		
		ParameterValueManagement.setString(result, "RequestSecurityTokenResponse/TokenType", "http://www.ws4d.org/wscompactsecurity/tokentypes#symmetric-key-token");
		ParameterValueManagement.setString(result, "RequestSecurityTokenResponse/RequestType", "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH2Response");
		ParameterValueManagement.setString(result, "RequestSecurityTokenResponse/RequestedSecurityToken/auth-ecc-dh-cmac", authentication.getNewTargetCMAC());
		
		return result;
	}

}
