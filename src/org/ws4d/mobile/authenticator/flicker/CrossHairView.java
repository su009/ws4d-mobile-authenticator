package org.ws4d.mobile.authenticator.flicker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.view.SurfaceView;

public class CrossHairView extends SurfaceView{

	private Paint mRectPaint;
	private RectF mInner;
	private RectF mOutter;
	
	int mH = -1;
	int mW = -1;
	
	public CrossHairView(Context context) {
		super(context);
		
		mRectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mRectPaint.setARGB(0xCC, 0xFF, 0, 0);
		mRectPaint.setStrokeWidth(3);
		mRectPaint.setStyle(Style.STROKE);
		
		mOutter = new RectF();
		mInner  = new RectF();
		
		setWillNotDraw(false);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		if (mH == -1)
			mH = this.getHeight(); 
		
		if (mW == -1)
			mW = this.getWidth();
		
		mOutter.set(mW/2-mW/10, mH/2-mH/10, mW/2+mW/10, mH/2+mH/10);
		mInner.set(mW/2-mW/40, mH/2-mH/40, mW/2+mW/40, mH/2+mH/40);
		
		canvas.drawRoundRect(mOutter, 10, 10, mRectPaint);
		canvas.drawRoundRect(mInner, 5, 5, mRectPaint);
		
		canvas.drawLine(mW/2, mH/2-mH/40, mW/2, mH/2-mH/5, mRectPaint);
		canvas.drawLine(mW/2, mH/2+mH/40, mW/2, mH/2+mH/5, mRectPaint);
		
		canvas.drawLine(mW/2-mW/40, mH/2, mW/2-mW/5, mH/2, mRectPaint);
		canvas.drawLine(mW/2+mW/40, mH/2, mW/2+mW/5, mH/2, mRectPaint);
		
	}
	
}
