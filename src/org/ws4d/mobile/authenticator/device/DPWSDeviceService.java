package org.ws4d.mobile.authenticator.device;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Random;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.mobile.authenticator.AuthenticatorOverview;
import org.ws4d.mobile.authenticator.R;
import org.ws4d.mobile.authenticator.TestOperationActivity;
import org.ws4d.mobile.authenticator.client.Client;
import org.ws4d.mobile.authenticator.flicker.FlickerAuthenticationPluginActivity;
import org.ws4d.mobile.authenticator.pin.PinAuthenticationPluginActivity;
import org.ws4d.wscompactsecurity.authentication.CallbackDataObject;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;
import org.ws4d.wscompactsecurity.authentication.test.TestOperationData;
import org.ws4d.wscompactsecurity.authentication.test.ToggleOperationData;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import android.widget.Toast;

//TODO: Come up w/ sth/ else e.g. use a lovely settings class!!
@SuppressLint("WorldReadableFiles")
public class DPWSDeviceService extends Service {

	public static final int MSG_TEST_CLIENT_REPORT_BACK = 1;
	public static final int MSG_AUTHENTICATION_CLIENT_REPORT_BACK = 2;
	private static int mRequestId = 1;
	
	private static TheDevice d = null;
	public static TheDevice getDevice() {
		return d;
	}
	boolean mWaitingForResult = false;
	boolean mWaitingForAuthenticationResult = false;
	boolean freshAuthenticationData = false;
	
	CallbackDataObject mClientOperationData = null;
	ActiveAuthentication mActiveAuthentication = null;
	
    private boolean isFakeDeviceActive() {
    	SharedPreferences settings = getSharedPreferences(AuthenticatorOverview.SHARED_PREFS_FILE, MODE_WORLD_READABLE);
    	return settings.getBoolean("fakeDeviceActive", false);
    }
    
    private QNameSet getFakePortTypes() {
    	//TODO: Read from Prefs File
    	return new QNameSet(new QName("LightbulbDevice", "http://www.ws4d.org"));
    }
	
	OperationDataCallback cb = new OperationDataCallback() {
		@Override
		public String testOperationSetData(CallbackDataObject data) {
			if (data != null) {
				TestOperationData toData = (TestOperationData) data.getObject();
				Log.d("ws4d", "in: " + toData.getIn() + "; out: " + toData.getOut());
				
				NotificationCompat.Builder mBuilder = new Builder(getApplicationContext());
				mBuilder.setSmallIcon(R.drawable.ic_launcher);
				mBuilder.setContentTitle("Received Toggle Request!");
				mBuilder.setContentText("in: " + toData.getIn() + "; out: " + toData.getOut());
				
				Intent intent = new Intent(getApplicationContext(), TestOperationActivity.class);
				intent.putExtra("URI", toData.getIn());
				
				mBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, intent, 0));
				
				mBuilder.setAutoCancel(true);
				mBuilder.setPriority(Notification.PRIORITY_HIGH);
				mBuilder.setDefaults(Notification.DEFAULT_ALL);
				
				NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.notify(1, mBuilder.build());
				
				mWaitingForResult = true;
				
				while (mClientOperationData == null) {
					/* I really feel bad for this but I desperately need to get this 
					 * working. Only thing I'm up to right now is leaving a FIXME!!
					 */
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				String result = ((ToggleOperationData)mClientOperationData).getResult();
				
				mClientOperationData = null;
				mWaitingForResult = false;
				
				return result;
				
			}
			return "";
		}

		@Override
		public ActiveAuthentication authenticationCallback1(
				ActiveAuthentication data) {
			
			/* Build the notification to inform the user */
			/*   compact form: */
			NotificationCompat.Builder mBuilder = new Builder(getApplicationContext());
			mBuilder.setSmallIcon(R.drawable.ic_launcher);
			mBuilder.setContentTitle("Authentication Request!");
			mBuilder.setContentText("in: " + data.getOrigin() + "; out: " + data.getTarget());
			
			/*   detailed form: */
			NotificationCompat.BigTextStyle style = new BigTextStyle();
			style.setBigContentTitle("New Authentication Request");
			style.bigText("Origin: " + data.getOrigin() + 
					"\n  (unknown type), ui: " + data.getOriginMechanism() +
					"\nTarget: " + data.getTarget() + 
					"\n  (" + data.getTargetDeviceType() + "), ui: " + data.getTargetMechanism());
			
			mBuilder.setStyle(style);
			
			/* assemble intent */
			
			Intent intent = null;
			
			if (isFakeDeviceActive()) {
				intent = new Intent(getApplicationContext(), PinAuthenticationPluginActivity.class);
				intent.putExtra("org.ws4d.authentication", data);
				intent.putExtra("fake", true);
				intent.putExtra("primary", true);
			} else if (data.getTargetMechanism().equals("http://www.ws4d.org/authentication/ui/mechanisms#flicker")) {
				
				intent = new Intent(getApplicationContext(), FlickerAuthenticationPluginActivity.class);
				intent.putExtra("org.ws4d.authentication", data);
				
			} else if (data.getTargetMechanism().equals("http://www.ws4d.org/authentication/ui/mechanisms#pin")) {
				intent = new Intent(getApplicationContext(), PinAuthenticationPluginActivity.class);
				intent.putExtra("org.ws4d.authentication",	data);
				intent.putExtra("fake", false);
				intent.putExtra("primary", true);
//			} else if (anotherMechanism) { /* here be more mechanisms */ 
				
			} else {
				/* maybe some error message ..? */
			}
			
			if (intent != null) {
				mBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), mRequestId++, intent, 0));
			}
			
			mBuilder.setAutoCancel(true);
			mBuilder.setPriority(Notification.PRIORITY_HIGH);
			mBuilder.setDefaults(Notification.DEFAULT_ALL);
			
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(1, mBuilder.build());
			
			mWaitingForAuthenticationResult = true;
			
			while (!freshAuthenticationData) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			freshAuthenticationData = false;
			
			return mActiveAuthentication;
		}

		@Override
		public ActiveAuthentication authenticationCallback2(
				ActiveAuthentication data) {
			return Client.authenticateECCDH(data, isFakeDeviceActive());
		}
	};
	
	static class IncomingHandler extends Handler {
		/* This one receives messages from client implementations.
		 * This way, client implementations can report back any results
		 */
		
		/* about that referencing thing:
		 * 
		 * Class needs to be static because otherwise memory leaks 
		 * are possible. By using the weak reference to the DPWSDeviceService
		 * object, it is still possible to refer to its members
		 */
		
		private WeakReference<DPWSDeviceService> mService = null;
		
		public IncomingHandler(DPWSDeviceService service) {
			mService = new WeakReference<DPWSDeviceService>(service);
		}
		
		@Override
		public void handleMessage(Message msg) {
			Bundle b = msg.getData();
			switch (msg.what) {
			case MSG_TEST_CLIENT_REPORT_BACK:
				
				ToggleOperationData data = (ToggleOperationData) b.getSerializable("data");
				if (mService.get().mWaitingForResult) {
					mService.get().mClientOperationData = data;
				}
				
				Log.d("WS4D", "Got Service Message! " + data.getResult());
				break;
			case MSG_AUTHENTICATION_CLIENT_REPORT_BACK:
				
				ActiveAuthentication aa = (ActiveAuthentication) b.getSerializable("org.ws4d.authentication");
				
				if (mService.get().mWaitingForAuthenticationResult) {
					mService.get().mActiveAuthentication = aa;
				}
				
				mService.get().freshAuthenticationData = true;
				
				Log.d("WS4D", "Got Authentication Data");
				break;
			default:
				super.handleMessage(msg);
				break;
			}
			
		}
	}
	final Messenger mMessenger = new Messenger(new IncomingHandler(this));
	
	public DPWSDeviceService() {
		super();
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		Random r = new Random(System.currentTimeMillis());
		mRequestId = r.nextInt();
	}



	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		if (!JMEDSFramework.isRunning()) { /* start framework and Device */
			JMEDSFramework.start(null);
		}
		
		if (d == null) {
			d = new TheDevice(cb, isFakeDeviceActive() ? getFakePortTypes() : null);
		}
		
		if (!d.isRunning()) {
			try {
				d.start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.d("ws4d", "Service started... I should be discoverable...");
			Toast.makeText(this, "Device started in background...", Toast.LENGTH_SHORT).show();
		}
		
		
		return START_STICKY;
	}
	

	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}



	@Override
	public void onDestroy() {
		try {
			if (d != null) {
				if (d.isRunning()) {
					d.stop();
				}
				d = null;
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JMEDSFramework.stop();
		JMEDSFramework.kill();
		Log.d("ws4d", "Service stawped... I should no longer be discoverable...");
		super.onDestroy();
	}


}
