package org.ws4d.mobile.authenticator;

import org.ws4d.mobile.authenticator.client.Client;
import org.ws4d.mobile.authenticator.device.DPWSDeviceService;
import org.ws4d.wscompactsecurity.authentication.test.ToggleOperationData;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TestOperationActivity extends Activity {

	String mURI;
	String mLastResult = "";
	
	Messenger mReportBackService = null;
	boolean mServiceBound = false;
	
	private ServiceConnection mConnection = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mReportBackService = null;
			mServiceBound = false;
			Log.d("WS4D", "TestOperationActivity: Unbound from Service");
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mReportBackService = new Messenger(service);
			mServiceBound = true;
			Log.d("WS4D", "TestOperationActivity: Bound to Service");
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_operation);
		
		TextView uriLabel = (TextView) findViewById(R.id.uriView);
		
		mURI = getIntent().getExtras().getString("URI", "????");
		
		uriLabel.setText(mURI);
		
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		bindService(new Intent(this, DPWSDeviceService.class), mConnection, Context.BIND_AUTO_CREATE);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if (mServiceBound) {
			unbindService(mConnection);
			mServiceBound = false;
		}
	}
	
	public void onLaunchButtonClicked(View v) {
		String result = Client.testOperationToggleLightBulb(mURI);
		
		mLastResult = result;
		
		TextView resultView = (TextView) findViewById(R.id.testOperationResultView);
		resultView.setText(result);
		
		Button b = (Button) findViewById(R.id.reportBackButton);
		b.setEnabled(true);
	}
	
	public void onReportButtonClicked(View v) {
		if (!mServiceBound) return;
		if (mLastResult.isEmpty()) return;
		
		ToggleOperationData data = new ToggleOperationData();
		data.setResult(mLastResult);
		
		Message msg = Message.obtain(null, DPWSDeviceService.MSG_TEST_CLIENT_REPORT_BACK);
		
		Bundle b = new Bundle();
		
		b.putSerializable("data", data);
		
		msg.setData(b);
		
		try {
			mReportBackService.send(msg);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.test_operation, menu);
		return true;
	}

}
