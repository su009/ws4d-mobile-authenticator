package org.ws4d.mobile.authenticator.tap;

import org.ws4d.mobile.authenticator.R;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class TapAuthenticationPluginActivity extends Activity {
	
	ImageView switchView = null;	
	TextView integerLabel = null;
	TextView binaryLabel = null;
	TextView keyLabel = null;
	Drawable switchOnImage = null;
	Drawable switchOffImage = null;
	Drawable readyImage = null;
	Drawable oneImage = null;
	Drawable twoImage = null;
	Drawable threeImage = null;
	Drawable goImage = null;
	Drawable currentImage = null;
	Drawable noSharedSecretImage = null;
	ProgressBar tapProgress = null;
	Button stopButton = null;
	Button startButton = null;	
	SeekBar keyBar = null;
	Bundle mBundle = null;
	ActiveAuthentication mActiveAuthentication = null;
	
	boolean runningModeEnabled = false;	
	boolean demoModeEnabled = false;
	Thread myThread = null;	
	String bin_key = "0";
	int demoKey = 349525;
	int shared_key = -1;
	int progressCount;
	final int symbol_time=1000;
	
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tap_authentication_plugin);
		
		tapProgress = (ProgressBar) findViewById(R.id.tap_progressBar);
		integerLabel = (TextView) findViewById(R.id.tap_IntegerLabel);
		binaryLabel = (TextView) findViewById(R.id.tap_BinaryLabel);
		keyLabel = (TextView) findViewById(R.id.tap_textView_key);
		switchView = (ImageView) findViewById(R.id.tap_imageView_switch);
		switchOnImage = getResources().getDrawable(R.drawable.tap_switch_on);
		switchOffImage = getResources().getDrawable(R.drawable.tap_switch_off);
		readyImage = getResources().getDrawable(R.drawable.tap_ready);
		oneImage = getResources().getDrawable(R.drawable.tap_1);
		twoImage = getResources().getDrawable(R.drawable.tap_2);
		threeImage = getResources().getDrawable(R.drawable.tap_3);
		goImage = getResources().getDrawable(R.drawable.tap_go);
		noSharedSecretImage = getResources().getDrawable(R.drawable.tap_nosecret);
		stopButton = (Button) findViewById(R.id.tap_button_stop);
		startButton = (Button) findViewById(R.id.tap_button_start);		
		
		setImage(readyImage);		
		
		keyBar = (SeekBar) findViewById(R.id.tap_seekBar_key);
		keyBar.setProgress(demoKey);		
		keyBar.setOnSeekBarChangeListener(seekBarChangeListener);
		stopButton.setEnabled(false);		
		enableDemoModeUi(true);
		
		mBundle= getIntent().getExtras();
		
		if (mBundle != null) {
			mActiveAuthentication = (ActiveAuthentication) mBundle.getSerializable("org.ws4d.authentication");
		}
	        
        if (mActiveAuthentication != null) {
        	if (!mBundle.getBoolean("primary", true)) {
        		
        		shared_key = mActiveAuthentication.getOOBSharedSecret();
				enableDemoModeUi(false);
				if (mBundle.getBoolean("fake", false)) {
					Toast.makeText(getApplicationContext(), "You are in Fake Mode", Toast.LENGTH_LONG).show();
				}
        	
	        }
        } else {
        	enableDemoModeUi(true);
        	Toast.makeText(getApplicationContext(), "You are in Demo Mode", Toast.LENGTH_LONG).show();
        }	
		
	}	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		/* stop thread when app gets closed */
		if (myThread != null) {
			myThread.interrupt();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tap_authentication_plugin, menu);
		return true;
	}
	
	
	public void setImageAndWait(Drawable image, int time) { 		
		currentImage=image;
		runOnUiThread(uiUpdateCurrentImage);				
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			return;
		}		
        return; 
    }
	
	public void setImage(Drawable image) { 		
		currentImage=image;
		runOnUiThread(uiUpdateCurrentImage);			
        return; 
    }
	
	
	 public String formatMitNullenLinks(String value, int len) { 
	        String result = String.valueOf(value); 
	        while (result.length() < len) { 
	            result = "0" + result; 
	        } 
	        return result; 
	    }
	
	
	
	public void onButtonStartClick(View v) {
		Log.d("hannes", "Button 1 Clicked");			
		if (myThread == null) { /* This way, it won't be started more that once */
			myThread = new Thread(threatStartTapFlickering);
			myThread.start();
		}
	}
	
	public void onButtonStopClick(View v) {
		Log.d("hannes", "Button 2 Clicked");
		runningModeEnabled = false;
		runOnUiThread(uiUpdateRunningModeUi);		
	}
	
	
	
	Thread threatStartTapFlickering = new Thread() {
	
		@Override
		public void run() {
		
			if(demoModeEnabled){
					bin_key=Integer.toBinaryString(demoKey);
				}else{
					bin_key=Integer.toBinaryString(shared_key);};     
					
					
				Log.d("hannes", bin_key);
				bin_key=formatMitNullenLinks(bin_key, 20);
				Log.d("hannes", bin_key);				
					
				
				runningModeEnabled = true;
				runOnUiThread(uiUpdateRunningModeUi);	
				
				startCountdown();	
				
				if(runningModeEnabled){setImageAndWait(switchOnImage, symbol_time);}     // startbit						
				
				for(progressCount=1;progressCount<=20;progressCount++){
					Log.d("hannes", "Count: " + Integer.toString(progressCount));
					runOnUiThread(uiUpdateProgessbar);
					if(runningModeEnabled){                            
						if(bin_key.charAt(progressCount-1)=='1'){
							setImageAndWait(switchOnImage, symbol_time);						
						}else{ 
							setImageAndWait(switchOffImage, symbol_time);
							};
					};
				};					
				
				progressCount = 0;
				runOnUiThread(uiUpdateProgessbar);
				setImage(readyImage);	
				runningModeEnabled = false;
				runOnUiThread(uiUpdateRunningModeUi);				
				myThread = null;			
			
		}
	};
	
	Runnable uiUpdateCurrentImage = new Runnable() {
		
		@Override
		public void run() {
        //			iv.setVisibility(visible ? View.VISIBLE : View.INVISIBLE); 
		switchView.setImageDrawable(currentImage);			
		}
	};
	
    
	Runnable uiUpdateRunningModeUi  = new Runnable() {
		
		@Override
		public void run() {
        startButton.setEnabled(!runningModeEnabled);
		stopButton.setEnabled(runningModeEnabled);
		keyBar.setEnabled(!runningModeEnabled);					
		}
	};
	
	Runnable uiUpdateProgessbar  = new Runnable() {
		
		@Override
		public void run() { 	
			
			tapProgress.setProgress(progressCount); 		
					
		}
	};
	
	
	OnSeekBarChangeListener seekBarChangeListener = new OnSeekBarChangeListener() {
		
		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			/* nothing to do at the precise moment when you put your thumb on that seek bar */
		}
		
		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			/* nothing to do at the precise moment when you release your thumb either... */
		}
		
		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			demoKey = progress;			
			integerLabel.setText("Integer: " + Integer.valueOf(demoKey).toString());			
			bin_key=Integer.toBinaryString(demoKey);			
			bin_key=formatMitNullenLinks(bin_key, 20);						
			binaryLabel.setText("Binary: " + bin_key);
			
		}
	};
	
	
	public void enableDemoModeUi(boolean enabled){			
		
		if(enabled){
			Log.d("hannes", "Demomode enabled");
			
			demoModeEnabled = true;
			
			keyBar.setVisibility(View.VISIBLE);
			keyLabel.setVisibility(View.VISIBLE);			
			binaryLabel.setVisibility(View.VISIBLE);
			
			demoKey = keyBar.getProgress();			
			integerLabel.setText("Integer: " + Integer.valueOf(demoKey).toString());			
			bin_key=Integer.toBinaryString(demoKey);			
			bin_key=formatMitNullenLinks(bin_key, 20);						
			binaryLabel.setText("Binary: " + bin_key);		
			
			setImage(readyImage);
			startButton.setEnabled(true);
		}	
		
		if(!enabled){
			Log.d("hannes", "Demomode disabled");
			
			demoModeEnabled = false;
			
			keyBar.setVisibility(View.GONE);
			keyLabel.setVisibility(View.GONE);			
			binaryLabel.setVisibility(View.GONE);
			
			if(shared_key==-1){			
				
				integerLabel.setText("Shared Key is: empty");	   
				setImage(noSharedSecretImage);				
				startButton.setEnabled(false);				
				
			}else{				
				integerLabel.setText("Shared Key is: " + Integer.toString(shared_key));	
				setImage(readyImage);	        	
	        	startButton.setEnabled(true);      	
	        	        				
			}
		}
		
	}
	
	public void startCountdown(){	
		if(runningModeEnabled){setImageAndWait(threeImage,symbol_time);}
		if(runningModeEnabled){setImageAndWait(twoImage,symbol_time);}
		if(runningModeEnabled){setImageAndWait(oneImage,symbol_time);}		
		if(runningModeEnabled){setImageAndWait(goImage,symbol_time);}		
	}
	
	
	
	

}


