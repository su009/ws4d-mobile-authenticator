package org.ws4d.mobile.authenticator.pin;

import org.ws4d.mobile.authenticator.AuthenticatorOverview;
import org.ws4d.mobile.authenticator.R;
import org.ws4d.mobile.authenticator.client.Client;
import org.ws4d.mobile.authenticator.device.DPWSDeviceService;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PinAuthenticationPluginActivity extends Activity {

	ActiveAuthentication mActiveAuthentication = null;
	
	Bundle mBundle = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pin_authentication_plugin);
		
		mBundle= getIntent().getExtras();
		
		if (mBundle != null) {
			mActiveAuthentication = (ActiveAuthentication) mBundle.getSerializable("org.ws4d.authentication");
		}
	        
        if (mActiveAuthentication != null) {
        	TextView tv = (TextView) findViewById(R.id.pinEditText);
        	if (!mBundle.getBoolean("primary", true)) {
	        	tv.setText(Integer.toString(mActiveAuthentication.getOOBSharedSecret()));
	        	tv.setEnabled(false);
        	}
        }

        if (mBundle != null) {
        
	        final Button launchButton = (Button) findViewById(R.id.pinLaunchButton);
	        final Button okButton = (Button) findViewById(R.id.pinOKbutton);
	    	
        	launchButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					launchButton.setEnabled(false);
					Thread t = new Thread(reqThread);
					t.start();
					Toast.makeText(getApplicationContext(), "Client request started. Pin should appear soon.", Toast.LENGTH_SHORT).show();
				}
			});

        	if (mBundle.getBoolean("fake", false)) {
	        	launchButton.setEnabled(true);
	        	Toast.makeText(getApplicationContext(), "You are in Fake Mode. Simply Press Launch!", Toast.LENGTH_LONG).show();
	        } else if (mBundle.getBoolean("primary", false)) {
	        	launchButton.setEnabled(true);
	        } else {
	        	okButton.setEnabled(true);
	        }
	        
	    	okButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					if (mBundle.getBoolean("primary", false)) {
						Intent i = new Intent(getApplicationContext(), AuthenticatorOverview.class);
						i.putExtra("org.ws4d.authentication", mActiveAuthentication);
						startActivity(i);
					}
					finish();
					
				}
			});
        }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pin_authentication_plugin, menu);
		return true;
	}

	private Runnable reqThread = new Runnable() {
		
		@Override
		public void run() {
			mActiveAuthentication = Client.authenticateECCDH(mActiveAuthentication, mBundle.getBoolean("fake", false));
			/* report back Target Reponse to device logic, so Origin gets the reponse */

			if (mServiceBound) {
			
				Message msg = Message.obtain(null, DPWSDeviceService.MSG_AUTHENTICATION_CLIENT_REPORT_BACK);
				
				Bundle b = new Bundle();
				
				b.putSerializable("org.ws4d.authentication", mActiveAuthentication);
				
				msg.setData(b);
				
				try {
					mReportBackService.send(msg);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			runOnUiThread(postReqThread);
		}
	};
	
	private Runnable postReqThread = new Runnable() {
		
		@Override
		public void run() {
			if (mActiveAuthentication.getErrorOccured()) {
				Toast.makeText(getApplicationContext(), "An Error occured!!\n\n" + mActiveAuthentication.getErrorMessage(), Toast.LENGTH_LONG).show();
			}
			if (mBundle.getBoolean("fake", false)) {
				TextView tv = (TextView) findViewById(R.id.pinEditText);
				tv.setText(Integer.valueOf(mActiveAuthentication.getOOBSharedSecret()).toString());
			}
			
			Button okButton = (Button) findViewById(R.id.pinOKbutton); 
			okButton.setEnabled(true);
			if (mBundle.getBoolean("fake", false)) {
				Toast.makeText(getApplicationContext(), "You are in fake mode. Please simply hit OK to report the fake PIN", Toast.LENGTH_LONG).show();
			}
		}
	};
	
	Messenger mReportBackService = null;
	boolean mServiceBound = false;
	
	/* Binding to the service is necessary for reporting request results back to device logic */
	private ServiceConnection mConnection = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mReportBackService = null;
			mServiceBound = false;
			Log.d("WS4D", "FlickerAuthenticationActivity: Unbound from Service");
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mReportBackService = new Messenger(service);
			mServiceBound = true;
			Log.d("WS4D", "FlickerAuthenticationActivity: Bound to Service");
		}
	};
	
	@Override
	protected void onStart() {
		super.onStart();
		bindService(new Intent(this, DPWSDeviceService.class), mConnection, Context.BIND_AUTO_CREATE);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if (mServiceBound) {
			unbindService(mConnection);
			mServiceBound = false;
		}
	}
	
}
