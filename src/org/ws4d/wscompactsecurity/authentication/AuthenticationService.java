package org.ws4d.wscompactsecurity.authentication;

import org.ws4d.java.service.DefaultService;
import org.ws4d.java.types.URI;
import org.ws4d.wscompactsecurity.authentication.Constants;
import org.ws4d.wscompactsecurity.authentication.eccdh.ECC_DH1;
import org.ws4d.wscompactsecurity.authentication.eccdh.ECC_DH2;
import org.ws4d.wscompactsecurity.authentication.test.TestOperation;

public class AuthenticationService extends DefaultService {

	public AuthenticationService() {
		this(null);
	}

	public AuthenticationService(OperationDataCallback cb) {
	
		super();
		
		this.setServiceId(new URI(Constants.AuthenticationServiceID));
		
		this.addOperation(new TestOperation(cb));
		
		/* ECC-DH */
		this.addOperation(new ECC_DH1(cb));
		this.addOperation(new ECC_DH2(cb));
		
	}

}
