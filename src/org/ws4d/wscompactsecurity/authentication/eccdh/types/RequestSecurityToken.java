package org.ws4d.wscompactsecurity.authentication.eccdh.types;

import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.types.QName;
import org.ws4d.wscompactsecurity.authentication.Constants;

public class RequestSecurityToken extends ComplexType {

	public RequestSecurityToken() {
		super(new QName("RequestSecurityToken", Constants.Namespace_WS_Trust), ComplexType.CONTAINER_SEQUENCE);
		
		Element tokenType = new Element(new QName("TokenType", Constants.Namespace_WS_Trust), SchemaUtil.TYPE_STRING);
		Element requestType = new Element(new QName("RequestType", Constants.Namespace_WS_Trust), SchemaUtil.TYPE_STRING);
		Element appliesTo = new Element(new QName("AppliesTo", Constants.Namespace_WS_Policy), SchemaUtil.TYPE_STRING);
		Element onBehalfOf = new Element(new QName("OnBehalfOf", Constants.Namespace_WS_Trust), SchemaUtil.TYPE_STRING);
		Element authenticationType = new Element(new QName("AuthenticationType", Constants.Namespace_WS_Trust), SchemaUtil.TYPE_STRING);
		Element reqSecToken = new Element(new QName("RequestedSecurityToken", Constants.Namespace_WS_Trust), new RequestedSecurityToken());
		
		this.addElement(tokenType);
		this.addElement(requestType);
		this.addElement(appliesTo);
		this.addElement(onBehalfOf);
		this.addElement(authenticationType);
		this.addElement(reqSecToken);
	}

}
