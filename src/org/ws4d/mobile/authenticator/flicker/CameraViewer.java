package org.ws4d.mobile.authenticator.flicker;

import java.io.IOException;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CameraViewer extends SurfaceView implements SurfaceHolder.Callback{

	private Camera mCamera;
	private SurfaceHolder mHolder;
	private int mPreviewWidth;
	private int mPreviewHeight;
	
	private BitStreamDetectorCallback mBitStreamDetectorCallback = null;
	
	public BitStreamDetectorCallback getBitStreamDetectorCallback() {
		return mBitStreamDetectorCallback;
	}

	public void setBitStreamDetectorCallback(
			BitStreamDetectorCallback bitStreamDetectorCallback) {
		this.mBitStreamDetectorCallback = bitStreamDetectorCallback;
	}

	@SuppressWarnings("deprecation")
	public CameraViewer(Context context, Camera camera) {
		super(context);
		mCamera = camera;
		
		mHolder = getHolder();
		mHolder.addCallback(this);
		
		mPreviewHeight = camera.getParameters().getPreviewSize().height;
		mPreviewWidth = camera.getParameters().getPreviewSize().width;
		
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
		if (mHolder.getSurface() == null)
			return;
		
		mCamera.stopPreview();
		
		try {
			mCamera.setPreviewDisplay(mHolder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
			mCamera.setDisplayOrientation(90);
		else
			mCamera.setDisplayOrientation(0);
		
		Parameters p = mCamera.getParameters();
		p.setExposureCompensation(0);
		mCamera.setParameters(p);
		
		mCamera.startPreview();
		mCamera.setPreviewCallback(mPreviewCallback);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		try {
			mCamera.setPreviewDisplay(holder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("CameraViewer", e.getMessage());
		}
		mCamera.startPreview();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}
	
	/* to following routine is called for EVERY FRAME displayed in the preview */
	private PreviewCallback mPreviewCallback = new PreviewCallback() {
		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			/* in NV21 format, the first width x height bytes of data
			 * contain the Y-data (which corresponds with grayscale image).
			 * The remaining (width x height) / 2 bytes contain color information 
			 */
			
			long luminosity = 0;
			long c=0;
			
			for (int row = mPreviewHeight / 2 - mPreviewHeight / 40 ; row < mPreviewHeight / 2 + mPreviewHeight / 40 ; row++) {
				for (int col = mPreviewWidth / 2 - mPreviewWidth / 40 ; col < mPreviewWidth / 2 + mPreviewWidth / 40 ; col++) {
					int v = (int)data[row * mPreviewWidth + col];
					luminosity += (v < 0) ? 256 + v : v;
					c++;
				}
			}
			
			short relativeLuminosity = (short) (luminosity / c);
			
			if (mBitStreamDetectorCallback != null)
				mBitStreamDetectorCallback.onLuminosityMeasured(relativeLuminosity);			
			
		}
	};
}
