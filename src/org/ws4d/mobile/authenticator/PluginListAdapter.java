package org.ws4d.mobile.authenticator;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class PluginListAdapter extends ArrayAdapter<PluginItem> {

	ArrayList<PluginItem> mPluginList = null;
	Context mContext;
	
	public PluginListAdapter(Context context, int textViewResourceId,
			ArrayList<PluginItem> pluginList) {
		super(context, textViewResourceId, pluginList);
		this.mContext = context;
		this.mPluginList = pluginList;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.plugin_item_view, null);
		}
		
		final PluginItem p = mPluginList.get(position);
		
		if (p != null) {
			TextView nameView = (TextView) v.findViewById(R.id.textName);
			if ((nameView != null) && (!p.getName().equals("")))
				nameView.setText(p.getName());
			
			TextView descriptionView = (TextView) v.findViewById(R.id.textDescription);
			if ((descriptionView != null) && (!p.getDescription().equals("")))
				descriptionView.setText(p.getDescription());
			
			ImageView iconView = (ImageView) v.findViewById(R.id.tap_imageView_switch);
			if ((iconView != null)&&(p.getIcon() != null)) {
				iconView.setImageDrawable(p.getIcon());
			}
			
			Button button = (Button) v.findViewById(R.id.launchTestOperationButton);
			if ((button != null)&&(!p.getButtonText().equals("")))
				button.setText(p.getButtonText());
			
			if (button != null) {
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (p.getIntent() != null)
							mContext.startActivity(p.getIntent());
					}
				});
			}
		}
		
		return v;
	}

}
