package org.ws4d.mobile.authenticator;

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class PluginTemplate extends PluginItem {

	public PluginTemplate(String name, String description, Intent intent) {
		super(name, description, intent);
	}

	public PluginTemplate(String name, String description, Drawable icon,
			String buttonText, Intent intent) {
		super(name, description, icon, buttonText, intent);
	}
	
	/*
	 * One fine day, there will be a real mechanism for dynamically installing
	 * and running additional functionality such as support for further 
	 * authentication mechanisms.
	 * 
	 * However, until this day has come, you might want to know how to hard-
	 * integrate your code.
	 * 
	 * Simply create the activity / component you want to be launched when 
	 * using your enhancement. Once done, hook in to 
	 * AuthenticatorOverview.registerPlugins() and add your Plugin by invoking
	 * something like
	 * 
	 * mPluginList.add(new PluginItem(
	 *			"Your Plugin Name",
	 *			"A somewhat longer (though short) description",
	 *			<An android drawable of the list items icon> (null is fine for default), 
	 *			"A custom button title" ("" is fine for default),
	 *			new Intent(this, YourPluginActivity.class)
	 *			));
	 * 
	 * For now, you are of course encouraged to use android resource system, so you
	 * can supply IDs and thus even easily localise your 'plugin' 
	 * 
	 */

}
