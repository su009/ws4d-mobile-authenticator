package org.ws4d.wscompactsecurity.authentication.test;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.wscompactsecurity.authentication.Constants;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;

import android.util.Log;

public class TestOperation extends Operation {

	private OperationDataCallback cb;

	public TestOperation() {
		this(null);
	}

	public TestOperation(OperationDataCallback cb) {

		super("TestOperation", new QName(Constants.AuthenticationServiceTestPortType, Constants.Namespace));
		
		Element inMsg = new Element(new QName("inMessage", Constants.Namespace), SchemaUtil.TYPE_STRING);
		Element out = new Element(new QName("response", Constants.Namespace), SchemaUtil.TYPE_STRING);
		
		setInput(inMsg);
		setOutput(out);
		
		this.cb = cb;
	}

	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {

		String in = ParameterValueManagement.getString(parameterValue, "inMessage");
		
		if (in == null) {
			in = "<none>";
		}
		
		Log.d("ws4d", in);
		
		ParameterValue response = createOutputValue();
		
		TestOperationData d = new TestOperationData(in, "");
		
		if (cb != null) {
			d.setOut(cb.testOperationSetData(d));
		} else {
			d.setOut("<ERR: No Callback Registered!>");
		}
		
		ParameterValueManagement.setString(response, "response", d.getOut());
				
		return response;
	}

}
