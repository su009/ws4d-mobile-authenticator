package org.ws4d.wscompactsecurity.authentication.eccdh;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.Element;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.wscompactsecurity.authentication.Constants;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;
import org.ws4d.wscompactsecurity.authentication.eccdh.types.RequestSecurityToken;
import org.ws4d.wscompactsecurity.authentication.eccdh.types.RequestSecurityTokenResponse;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;
import org.ws4d.wscompactsecurity.authentication.engine.AuthenticationEngine;

import android.util.Log;

public class ECC_DH1 extends Operation {

	OperationDataCallback cb = null;
	
	public ECC_DH1() {
		this(null);
	}
	
	public ECC_DH1(OperationDataCallback cb) {
		super("ECC_DH1", new QName(Constants.AuthenticationServiceECCDHPortType, Constants.Namespace));
		this.cb = cb;
		
		Element in = new Element(new QName("RequestSecurityToken", Constants.Namespace_WS_Trust), new RequestSecurityToken());
		Element out = new Element(new QName("RequestSecurityTokenResponse", Constants.Namespace_WS_Trust), new RequestSecurityTokenResponse());
		
		this.setInput(in);
		this.setOutput(out);
		
	}

	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {
		
		/* TODO: Receive Metadata from target to find out
		 * about supported authentication techniques 
		 * as well as device's information
		 * *********************************************** */
		
		String targetAuthenticationMechanism = "http://www.ws4d.org/authentication/ui/mechanisms#flicker";
		AuthenticationEngine engine = AuthenticationEngine.getInstance();
		
		ActiveAuthentication authentication = engine.getActiveAuthenticationByOrigin(ParameterValueManagement.getString(parameterValue, "OnBehalfOf"));
		
		if (authentication != null) {
			Log.d("ECC_DH1", "Found old record!");
			if (engine.removeActiveAuthenticationByOrigin(ParameterValueManagement.getString(parameterValue, "OnBehalfOf"))) {
				Log.d("ECC_DH1", "Could remove record. Remaining: " + engine.remainingActiveAuthentications());
			} else {
				Log.w("ECC_DH1", "Could NOT remove record. Remaining: " + engine.remainingActiveAuthentications());
			}
				
		} 
		
		authentication = new ActiveAuthentication();
		
		authentication.setOrigin(ParameterValueManagement.getString(parameterValue, "OnBehalfOf"));
		authentication.setTarget(ParameterValueManagement.getString(parameterValue, "AppliesTo"));
		authentication.setOriginMechanism(ParameterValueManagement.getString(parameterValue, "AuthenticationType"));
		authentication.setTargetMechanism(targetAuthenticationMechanism); /* TODO: (see above) */
		authentication.setTargetDeviceType("Light Bulb");
		
		
		
		authentication = cb.authenticationCallback1(authentication);
		
		ParameterValue result = createOutputValue();
		
		ParameterValueManagement.setString(result, "TokenType", "http://www.ws4d.org/wscompactsecurity/tokentypes#symmetric-key-token");
		ParameterValueManagement.setString(result, "RequestType", "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH1Response");
		ParameterValueManagement.setString(result, "AppliesTo", authentication.getOrigin());
		ParameterValueManagement.setString(result, "OnBehalfOf", authentication.getTarget());
		ParameterValueManagement.setString(result, "AuthenticationType", authentication.getOriginMechanism());
		
		ParameterValueManagement.setString(result, "RequestedSecurityToken/auth-ecc-dh-curvename", authentication.getCurveName());
		ParameterValueManagement.setString(result, "RequestedSecurityToken/auth-ecc-dh-nonce", authentication.getTargetNonce());
		ParameterValueManagement.setString(result, "RequestedSecurityToken/auth-ecc-dh-public-key", authentication.getTargetPublic());
		
		engine.addActiveAuthentication(authentication);
		return result;
	}

}
