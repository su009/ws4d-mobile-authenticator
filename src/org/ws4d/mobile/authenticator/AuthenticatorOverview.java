package org.ws4d.mobile.authenticator;

import java.util.ArrayList;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.mobile.authenticator.device.DPWSDeviceService;
import org.ws4d.mobile.authenticator.flicker.FlickerAuthenticationPluginActivity;
import org.ws4d.mobile.authenticator.pin.PinAuthenticationPluginActivity;
import org.ws4d.mobile.authenticator.tap.TapAuthenticationPluginActivity;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class AuthenticatorOverview extends Activity {

	ArrayList<PluginItem> mPluginList = null;
	PluginListAdapter mPluginListAdapter = null;
	
	ActiveAuthentication mActiveAuthentication = null;
	
	public static String SHARED_PREFS_FILE;
	
	public boolean isDeviceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (DPWSDeviceService.class.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authenticator_overview);
        
        mPluginList = new ArrayList<PluginItem>();
        
        mPluginListAdapter = new PluginListAdapter(this, R.layout.plugin_item_view, mPluginList);
        
        ListView lv = (ListView) findViewById(R.id.pluginListView);
        lv.setAdapter(mPluginListAdapter);
        
        registerPlugins();
        
        mPluginListAdapter.notifyDataSetChanged();
        
        if (!isDeviceRunning()) {
	        Intent deviceServiceIntent = new Intent(this, DPWSDeviceService.class);
	        startService(deviceServiceIntent);
        }
        
        Bundle bundle = getIntent().getExtras();
        
        if (bundle != null) {
                mActiveAuthentication = (ActiveAuthentication) bundle.getSerializable("org.ws4d.authentication");
        }
        
        if (mActiveAuthentication != null) {
        	Button b = (Button) findViewById(R.id.overviewProcessButton);
        	b.setVisibility(Button.VISIBLE);
        	b.setBackgroundResource(R.drawable.button_background);
        	b.setTextColor(Color.WHITE);
        	b.setOnClickListener(new OnClickListener() {

        		@Override
				public void onClick(View v) {
        			
        			if (mActiveAuthentication.getOriginMechanism().equals("http://www.ws4d.org/authentication/ui/mechanisms#pin")) {
        			
        				Intent i = new Intent(getApplicationContext(), PinAuthenticationPluginActivity.class);
        				i.putExtras(getIntent());
        				i.putExtra("primary", false);
        				startActivity(i);
        				finish();
        				
       			} else if (mActiveAuthentication.getOriginMechanism().equals("http://www.ws4d.org/authentication/ui/mechanisms#tap")) {
        			
        				Intent i = new Intent(getApplicationContext(), TapAuthenticationPluginActivity.class);
        				i.putExtras(getIntent());
        				i.putExtra("primary", false);
        				startActivity(i);
        				finish();
        				
//        			} else if () { Something else	
        				
        			}
        			
				}
			});
        }
        
    }

    private void registerPlugins() {
		//TODO: Replace by *real* plugin mechanism (one day in the very distant future)
    	mPluginList.add(new PluginItem(
				getResources().getString(R.string.flicker_plugin_name),
				getResources().getString(R.string.flicker_plugin_description),
				getResources().getDrawable(R.drawable.icon_light_bulb), 
				"",
				new Intent(this, FlickerAuthenticationPluginActivity.class)
				));
    	mPluginList.add(new PluginItem(
    			"QR Code Authentication", 
    			"Authenticate via a display QR Code", 
    			getResources().getDrawable(R.drawable.icon_qr_code), 
    			"", 
    			null
    			));
    	mPluginList.add(new PluginItem(
    			"NFC Authentication",
    			"Use near field communication to authenticate", 
    			getResources().getDrawable(R.drawable.icon_nfc), 
    			"", 
    			null
    			));
    	mPluginList.add(new PluginItem(
    			"USB Authentication", 
    			"Use a USB connection to authenticate", 
    			getResources().getDrawable(R.drawable.icon_usb), 
    			"", 
    			null
    			));
    	mPluginList.add(new PluginItem(
    			"PIN Authentication", 
    			"Authenticate by a displayed PIN", 
    			getResources().getDrawable(R.drawable.icon_pin),
    			"",
    			new Intent(getApplicationContext(), PinAuthenticationPluginActivity.class)
    			));    	
    	mPluginList.add(new PluginItem(
    			"Tap Authentication", 
    			"Authenticate via Tapping a Switch", 
    			getResources().getDrawable(R.drawable.icon_switch), 
    			"", 
    			new Intent(getApplicationContext(), TapAuthenticationPluginActivity.class)
    			));
    	
//    	mPluginList.add(new PluginTemplate("name","description", intent)); /* check class file PluginTemplate for documentation */
	}

    //TODO: Come up w/ sth/ else  e.g. use a lovely settings class!!
    @SuppressLint("WorldReadableFiles")
	private boolean isFakeDeviceActive() {
    	SharedPreferences settings = getSharedPreferences(SHARED_PREFS_FILE, MODE_WORLD_READABLE);
    	return settings.getBoolean("fakeDeviceActive", false);
    }
    
    //TODO: Come up w/ sth/ else e.g. use a lovely settings class!!
    @SuppressLint("WorldReadableFiles")
	private void setFakeDeviceActive(boolean a) {
    	SharedPreferences settings = getSharedPreferences(SHARED_PREFS_FILE, MODE_WORLD_READABLE);
    	SharedPreferences.Editor editor = settings.edit();
    	editor.putBoolean("fakeDeviceActive", a);
    	editor.commit();
    	
    	if (DPWSDeviceService.getDevice()!=null) { /* I think it's unlikely, but who knows..? */
	    	if (a) {
	    		DPWSDeviceService.getDevice().setAdditionalPortTypes(getFakeDevicePortTypes());
	    	} else {
	    		DPWSDeviceService.getDevice().resetPortTypes();
	    	}
    	}
    	
    }

    private QNameSet getFakeDevicePortTypes() {
		// TODO: Read from shared Preferences
		return new QNameSet(new QName("LightbulbDevice", "http://www.ws4d.org"));
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_authenticator_overview, menu);
        return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

    	menu.removeItem(R.id.menu_start_stop_fake_device);
    	
    	menu.add(Menu.NONE, R.id.menu_start_stop_fake_device, 100, isFakeDeviceActive() ? R.string.menu_stop_fake_device : R.string.menu_start_fake_device);
    	        
    	return super.onPrepareOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case R.id.menu_settings:
    		return true;
    	case R.id.menu_start_stop_service:
    		Intent deviceServiceIntent = new Intent(this, DPWSDeviceService.class);
	        if (!isDeviceRunning()) {
            	startService(deviceServiceIntent);
            	item.setTitle(R.string.menu_stop_service);
            	Toast.makeText(getApplicationContext(), "(Re)started Device", Toast.LENGTH_SHORT).show();
            } else {
            	stopService(deviceServiceIntent);
            	item.setTitle(R.string.menu_start_service);
            	Toast.makeText(getApplicationContext(), "Stopped Device", Toast.LENGTH_SHORT).show();
            }
	        return true;
    	case R.id.menu_start_stop_fake_device:
    		
    		if (isFakeDeviceActive()) {
    			Toast.makeText(getApplicationContext(), "Deactivated Fake Device", Toast.LENGTH_SHORT).show();
    			setFakeDeviceActive(false);
    			item.setTitle(R.string.menu_start_fake_device);
    		} else {
    			Toast.makeText(getApplicationContext(), "Started Fake Device\n\nATTENTION!!\n\nAll requests are handled by fake device now!!!", Toast.LENGTH_LONG).show();
    			setFakeDeviceActive(true);
    			item.setTitle(R.string.menu_stop_fake_device);
    		}
    		
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
    
}
