package org.ws4d.mobile.authenticator;

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class PluginItem {
	String mName;
	String mDescription;
	Drawable mIcon;
	String mButtonText;
	Intent mIntent;
	
	
	
	public Intent getIntent() {
		return mIntent;
	}

	public void setIntent(Intent intent) {
		this.mIntent = intent;
	}

	public Drawable getIcon() {
		return mIcon;
	}

	public void setIcon(Drawable icon) {
		this.mIcon = icon;
	}

	public String getButtonText() {
		return mButtonText;
	}

	public void setButtonText(String buttonText) {
		this.mButtonText = buttonText;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public String getDescription() {
		return mDescription;
	}

	public void setDescription(String description) {
		this.mDescription = description;
	}

	public PluginItem(String name, String description, Intent intent) {
		this(name, description, null, "", intent);
	}
	
	public PluginItem(String name, String description, Drawable icon, String buttonText, Intent intent) {
		mName = name;
		mDescription = description;
		mIcon = icon;
		mButtonText = buttonText;
		mIntent = intent;
	}
	
}
