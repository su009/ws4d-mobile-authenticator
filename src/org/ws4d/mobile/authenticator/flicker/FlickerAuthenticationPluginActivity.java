package org.ws4d.mobile.authenticator.flicker;

import org.ws4d.mobile.authenticator.AuthenticatorOverview;
import org.ws4d.mobile.authenticator.R;
import org.ws4d.mobile.authenticator.client.Client;
import org.ws4d.mobile.authenticator.device.DPWSDeviceService;
import org.ws4d.wscompactsecurity.authentication.engine.ActiveAuthentication;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

public class FlickerAuthenticationPluginActivity extends Activity {

	private Camera mCamera = null;
	private CameraViewer mCameraView;
	private BitStreamDetector mBitStreamDetector = new BitStreamDetector();
	private BitView mBitView;
	
	ActiveAuthentication mCurrentAuthentication = null;
	
	Boolean mLastSymbol;
	int mLastState;
	Boolean[] mLastKey;
	
	Messenger mReportBackService = null;
	boolean mServiceBound = false;
	
	/* Binding to the service is necessary for reporting request results back to device logic */
	private ServiceConnection mConnection = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mReportBackService = null;
			mServiceBound = false;
			Log.d("WS4D", "FlickerAuthenticationActivity: Unbound from Service");
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mReportBackService = new Messenger(service);
			mServiceBound = true;
			Log.d("WS4D", "FlickerAuthenticationActivity: Bound to Service");
		}
	};
	
	/* Operations to be performed on UI after BitStreamDetector read symbols */
	protected Runnable addSymbol = new Runnable() {
		
		@Override
		public void run() {
			mBitView.addSymbol(mLastSymbol);
		}
	};
	protected Runnable changeState = new Runnable() {
		
		@Override
		public void run() {
			mBitView.setState(mLastState);
		}
	};
	protected Runnable keyRead = new Runnable() {
		@Override
		public void run() {
			mBitView.fullKeyRead(mLastKey);
			Button b = (Button) findViewById(R.id.reportButton);
			b.setEnabled(true);
			if (mCurrentAuthentication != null) {
				if (mCurrentAuthentication.getOOBSharedSecret() == -1) { /* do only for the first key reported */
					int result = 0;
					for (int i = 0; i < mLastKey.length; i++) {
						result += ((mLastKey[i]) ? 1 : 0) * (1 << (mLastKey.length-1-i));
					}
					mCurrentAuthentication.setOOBSharedSecret(result);
				}
			}
		}
	};
	protected Runnable reset = new Runnable() {
		
		@Override
		public void run() {
			mBitView.reset();
		}
	};
	
	/* Callbacks for keys / symbols read by BitStreamDetector */
	BitStreamDetectorKeyReadCallback mBitStreamDetectorKeyReadCallback = new BitStreamDetectorKeyReadCallback() {
		@Override
		public void onSymbolRead(Boolean symbol) {
			mLastSymbol = symbol;
			runOnUiThread(addSymbol);
		}
		
		@Override
		public void onStateChanged(int state) {
			mLastState = state;
			runOnUiThread(changeState);
		}
		
		@Override
		public void onKeyRead(Boolean[] key) {
			mLastKey = key;
			runOnUiThread(keyRead);
		}

		@Override
		public void onReset() {
			runOnUiThread(reset);
		}
	};

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flicker_authentication_plugin);
        
        Bundle bundle = getIntent().getExtras();
        
        if (bundle != null) {
        	this.mCurrentAuthentication = (ActiveAuthentication) bundle.getSerializable("org.ws4d.authentication");
        }
        
        if (this.mCurrentAuthentication != null) {
        	Button b = (Button) findViewById(R.id.launchButton);
        	b.setEnabled(true);
        }
        
        mBitView = (BitView) findViewById(R.id.bitView1);
        
        if (!checkForCameraHardware(this)) {
        	Toast.makeText(this, getResources().getString(R.string.err_no_camera), Toast.LENGTH_LONG).show();
        	Button b = (Button) findViewById(R.id.launchButton);
        	b.setEnabled(false);
        	b = (Button) findViewById(R.id.reportButton);
        	b.setEnabled(false);
        	return;
        } 
        
        mCamera = getCameraInstance();
        
        if (mCamera == null) {
        	Button b = (Button) findViewById(R.id.launchButton);
        	b.setEnabled(false);
        	b = (Button) findViewById(R.id.reportButton);
        	b.setEnabled(false);
        	return;
        } 
        	
        mCameraView = new CameraViewer(this, mCamera);
        
        /* CameraViewer reports EVERY SINGLE PREVIEW FRAME to the BiutStreamCollectorCallback supplied below */
        mCameraView.setBitStreamDetectorCallback(mBitStreamDetector);
        
        mBitStreamDetector.setBitStreamDetectorKeyReadCallback(mBitStreamDetectorKeyReadCallback);
        
        FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
        
        preview.addView(mCameraView);
        
        /* overlay cross hair */
        preview.addView(new CrossHairView(this));
        
    }
	
	@Override
	protected void onStart() {
		super.onStart();
		bindService(new Intent(this, DPWSDeviceService.class), mConnection, Context.BIND_AUTO_CREATE);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if (mServiceBound) {
			unbindService(mConnection);
			mServiceBound = false;
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (mCamera != null) {
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

    private Camera getCameraInstance() {
    	Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
        	Toast.makeText(this, getResources().getString(R.string.err_no_camera_availlable), Toast.LENGTH_LONG).show();
        }
        return c; // returns null if camera is unavailable
	}

	private boolean checkForCameraHardware(Context ctx) {
		return ctx.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_flicker_authentication_plugin, menu);
        return true;
    }

	public void onLaunchButtonPressed(View v) {
		Button b = (Button) findViewById(R.id.launchButton);
		b.setEnabled(false);
		Thread t = new Thread(reqThread);
		
		t.start();
		
		Toast.makeText(getApplicationContext(), "Client request started. Flickering will begin shortly.", Toast.LENGTH_LONG).show();
		
	}
	
	public void onReportButtonPressed(View v) {
		Intent i = new Intent(getApplicationContext(), AuthenticatorOverview.class);
		i.putExtra("org.ws4d.authentication", mCurrentAuthentication);
		startActivity(i);
		finish();
	}
	
	private Runnable reqThread = new Runnable() {
		
		@Override
		public void run() {
			mCurrentAuthentication = Client.authenticateECCDH(mCurrentAuthentication, false);
			/* report back Target Reponse to device logic, so Origin gets the reponse */

			if (mServiceBound) {
			
				Message msg = Message.obtain(null, DPWSDeviceService.MSG_AUTHENTICATION_CLIENT_REPORT_BACK);
				
				Bundle b = new Bundle();
				
				b.putSerializable("org.ws4d.authentication", mCurrentAuthentication);
				
				msg.setData(b);
				
				try {
					mReportBackService.send(msg);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			runOnUiThread(postReqThread);
		}
	};
	
	private Runnable postReqThread = new Runnable() {
		
		@Override
		public void run() {
			if (mCurrentAuthentication.getErrorOccured()) {
				Toast.makeText(getApplicationContext(), "An Error occured!!\n\n" + mCurrentAuthentication.getErrorMessage(), Toast.LENGTH_LONG).show();
			} 
		}
	};
}
