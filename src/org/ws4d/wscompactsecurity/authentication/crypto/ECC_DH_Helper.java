package org.ws4d.wscompactsecurity.authentication.crypto;

/*
 * This 'Wrapper' Class was supposed to become something to help
 * us develop authentication protocols that basically depend on some sort
 * of ECC Diffie Hellman. While it (somehow) accomplishes this task, I'm
 * afraid that his class became a collection of anti-patterns rather than
 * being good coding style.
 * 
 * You may, of course, use it. But you should not consider it as 
 * inspiration for your own software development
 *  
 */

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;

import javax.crypto.KeyAgreement;
import org.spongycastle.crypto.engines.AESFastEngine;
import org.spongycastle.crypto.macs.CMac;
import org.spongycastle.crypto.params.KeyParameter;
import org.spongycastle.jce.provider.BouncyCastleProvider;

import android.util.Base64;
import android.util.Log;

public class ECC_DH_Helper {

	private AuthenticatedECCDHParameters mParams;
	private boolean initialized;
	private boolean beHoCompliant = false;
	
	public boolean isHoCompliant() {
		return beHoCompliant;
	}

	public void setHoCompliant(boolean beHoCompliant) {
		this.beHoCompliant = beHoCompliant;
	}

	public static PublicKey decodePublicKey(byte[] encodedPublicKey) {
		KeyFactory fact;
		try {
			fact = KeyFactory.getInstance("ECDSA", "SC");
			return (PublicKey) fact.generatePublic(new X509EncodedKeySpec(encodedPublicKey));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public ECC_DH_Helper(AuthenticatedECCDHParameters p) {
		/* in case you want to reuse key material */
		mParams = p;
		initialized = false;
	}
	
	public ECC_DH_Helper() {
		mParams = new AuthenticatedECCDHParameters();
		initialized = false;
	}
	
	public AuthenticatedECCDHParameters getParams() {
		return mParams;
	}

	public void setParams(AuthenticatedECCDHParameters params) {
		this.mParams = params;
	}

	public void init() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
		init("secp256r1"); /* Default as spcecified in IEEE 802.15.6-2012 */
	}
	
	public void init(String curveName) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
		
		if (!initialized) {
			
			mParams.setCurveName(curveName);
		
			boolean addProvider = true;
			
			for (Provider p : Security.getProviders()) {
				if (p.getName().equals(BouncyCastleProvider.PROVIDER_NAME)) {
					addProvider = false;
					break;
				}
			}
			
			if (addProvider) {
				Security.addProvider(new BouncyCastleProvider());
			}
			
			if ((mParams.getCurveName() != null) && (!mParams.getCurveName().isEmpty())) {
				
				ECGenParameterSpec ecGenSpec = new ECGenParameterSpec(mParams.getCurveName());
				KeyPairGenerator g = KeyPairGenerator.getInstance("ECDSA", "SC");
				
				g.initialize(ecGenSpec, new SecureRandom());
				
				if (mParams.getKeyPair() == null) { /* don't overwrite in case there already is any */
					mParams.setKeyPair(g.generateKeyPair());
				}
				initialized = true;
			} else {
				initialized = false;
			}
		}
		mParams.getOtherA().clear();
		mParams.getOtherB().clear();
		mParams.addOtherA(curveName);
		mParams.addOtherB(curveName);
	}
	
	public void setForeignPublicKey(PublicKey k) {
		mParams.setForeignPublicKey(k);
	}
	
	public void calculateSharedSecret() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
		calculateSharedSecret(mParams.getForeignPublicKey());
	}
	
	public void calculateSharedSecret(PublicKey priv) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
		KeyAgreement ka = KeyAgreement.getInstance("ECDH", "SC");
		ka.init(mParams.getKeyPair().getPrivate());
		ka.doPhase(mParams.getForeignPublicKey(), true);
		mParams.setSessionKey(ka.generateSecret());
	}
	
	public void OOBkey2Integer() {
		/* Leftmost bits become MSBs */
		int result = 0;
		for (int i = 0; i < mParams.getOOBSharedSecret().length; i++) {
			result += ((mParams.getOOBSharedSecret()[i]) ? 1 : 0) * (1 << (mParams.getOOBSharedSecret().length-1-i));
		}
		mParams.setOOBSharedSecretAsInt(result);
	}
	
	public void OOBkeyFromInteger() {
		boolean[] key = new boolean[20]; //FIXME -,-'
		int ikey = mParams.getOOBSharedSecretAsInt();
		for (int i=0; i < key.length; i++) {
			key[i] = (ikey & (1 << (20-i-1))) > 0;
		}
		mParams.setOOBSharedSecret(key);
	}
	
	public void encryptPublicKey() {
		if (beHoCompliant) {
			/* TODO: Real Encryption */
			mParams.setDistortedPublicKey(mParams.getKeyPair().getPublic());
		} else {
			mParams.setDistortedPublicKey(mParams.getKeyPair().getPublic());
		}
	}
	
	public void decryptPublicKey() {
		if (beHoCompliant) {
			/* TODO: Alike: real decryption */
			mParams.setForeignPublicKey(mParams.getDistortedPublicKey());
		} else {
			mParams.setForeignPublicKey(mParams.getDistortedPublicKey());
		}
	}
	
	public void calculateCMAC() {
		CMac cmac = new CMac(new AESFastEngine(), 64);
		
		KeyParameter key = null;
		
		if (beHoCompliant) {
			byte[] dhKeyPart = new byte[16];
			for (int i=0; i < 16; i++) {
				dhKeyPart[i] = mParams.getSessionKey()[16+i]; /* Right-most 128 bits */
			}
			
			key = new KeyParameter(dhKeyPart);
		} else {
			byte[] keyArray = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; /* ouch... */
			keyArray[15] = (byte) (mParams.getOOBSharedSecretAsInt() & 0xFF);
			keyArray[14] = (byte) ((mParams.getOOBSharedSecretAsInt() >> 8)  & 0xFF);
			keyArray[13] = (byte) ((mParams.getOOBSharedSecretAsInt() >> 16) & 0xFF);
			key = new KeyParameter(keyArray);
		}
		
		System.out.print("Key:");
		for (int i = 0; i < key.getKey().length; i++) {
			System.out.printf(" %02x", key.getKey()[i]);
		}
		System.out.print("\n");
		
		cmac.init(key);
		
		cmac.update(mParams.getOrigin().getBytes(), 0, mParams.getOrigin().getBytes().length);
		Log.d("calculateCMAC", "calculateCMAC: Origin: " + mParams.getOrigin());
		cmac.update(mParams.getTarget().getBytes(), 0, mParams.getTarget().getBytes().length);
		Log.d("calculateCMAC", "calculateCMAC: Target: " + mParams.getTarget());
		
		cmac.update(mParams.getNonceA(), 0, mParams.getNonceA().length);
		Log.d("calculateCMAC", "calculateCMAC: Origin Nonce: " + Base64.encodeToString(mParams.getNonceA(), Base64.DEFAULT));
		cmac.update(mParams.getNonceB(), 0, mParams.getNonceB().length);
		Log.d("calculateCMAC", "calculateCMAC: Target Nonce: " + Base64.encodeToString(mParams.getNonceB(), Base64.DEFAULT));
		
		String otherA = mParams.otherAasString();
		Log.d("calculateCMAC", "calculateCMAC: Other: " + otherA);
		
		cmac.update(otherA.getBytes(), 0, otherA.getBytes().length);
		
		byte[] out = new byte[8];
		
		cmac.doFinal(out, 0);
		
		mParams.setCMAC(out);
		
	}
	
	public boolean checkCMAC() {
		
		CMac cmac = new CMac(new AESFastEngine(), 64);
		
		KeyParameter key = null;
		
		if (beHoCompliant) {
			byte[] dhKeyPart = new byte[16];
			for (int i=0; i < 16; i++) {
				dhKeyPart[i] = mParams.getSessionKey()[16+i]; /* Right-most 128 bits */
			}
			
			key = new KeyParameter(dhKeyPart);
		} else {
			byte[] keyArray = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; /* ouch... */
			keyArray[15] = (byte) (mParams.getOOBSharedSecretAsInt() & 0xFF);
			keyArray[14] = (byte) ((mParams.getOOBSharedSecretAsInt() >> 8)  & 0xFF);
			keyArray[13] = (byte) ((mParams.getOOBSharedSecretAsInt() >> 16) & 0xFF);
			key = new KeyParameter(keyArray);
		}
		
		System.out.print("Key:");
		for (int i = 0; i < key.getKey().length; i++) {
			System.out.printf(" %02x", key.getKey()[i]);
		}
		System.out.print("\n");
		
		cmac.init(key);
		
		cmac.update(mParams.getTarget().getBytes(), 0, mParams.getTarget().getBytes().length);
		System.out.println("CheckCMAC: Origin: " + mParams.getTarget());
		cmac.update(mParams.getOrigin().getBytes(), 0, mParams.getOrigin().getBytes().length);
		System.out.println("CheckCMAC: Target: " + mParams.getOrigin());
		
		cmac.update(mParams.getNonceB(), 0, mParams.getNonceB().length);
		System.out.println("CheckCMAC: Origin Nonce: " + Base64.encodeToString(mParams.getNonceB(), Base64.DEFAULT));
		cmac.update(mParams.getNonceA(), 0, mParams.getNonceA().length);
		System.out.println("CheckCMAC: Target Nonce: " + Base64.encodeToString(mParams.getNonceA(), Base64.DEFAULT));
		
		String otherB = mParams.otherBasString();
		
		cmac.update(otherB.getBytes(), 0, otherB.getBytes().length);
		
		System.out.println("CheckCMAC: Other: " + otherB);
		
		byte[] out = new byte[8];
		
		cmac.doFinal(out, 0);
		
		for (int i = 0; i < mParams.getForeignCMAC().length; i++) {
			if (mParams.getForeignCMAC()[i] != out[i])
				return false;
		}
		
		return true;
				
	}
	
	public void calculateMasterKey() {
		CMac cmac = new CMac(new AESFastEngine(), 128);
		
		byte[] dhKeyPart = new byte [16];
		
		for (int i=0 ; i < 16; i++) {
			dhKeyPart[i] = mParams.getSessionKey()[i]; /* Left-most 128 bits */
		}
		
		KeyParameter key = new KeyParameter(dhKeyPart);
		
		cmac.init(key);
		
		byte[] material = new byte[16]; /* 128 bit Nonces XORed */
		
		byte[] nA = mParams.getNonceA();
		byte[] nB = mParams.getNonceB();
		
		for (int i = 0; i < 16; i++) {
			material[i] = (byte) (nA[i] ^ nB[i]);
		}
		
		cmac.update(material, 0, material.length);
		
		byte[] out = new byte[16];
		
		cmac.doFinal(out, 0);
		
		mParams.setMasterKey(out);
		
	}
	
	public void createNonce() {
		byte[] nonce = new byte[16];
		Random r = new Random();
		r.nextBytes(nonce);
		mParams.setNonceA(nonce);
		/* TODO: Maybe, cryptographically secure random numbers might be useful */
	}

}
