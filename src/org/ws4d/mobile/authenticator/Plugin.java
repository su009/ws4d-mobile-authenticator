package org.ws4d.mobile.authenticator;

import android.graphics.drawable.Drawable;

public interface Plugin {
	String mName = "";
	String mDescription = "";
	Drawable mIcon = null;
	String mButtonText = "";
	String getName();
	String getDescription();
	Drawable getIcon();
	String getButtonText();
}
