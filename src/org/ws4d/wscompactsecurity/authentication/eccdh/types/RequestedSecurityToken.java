package org.ws4d.wscompactsecurity.authentication.eccdh.types;

import org.ws4d.java.constants.SchemaConstants;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.ExtendedSimpleContent;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.types.QName;
import org.ws4d.wscompactsecurity.authentication.Constants;

public class RequestedSecurityToken extends ComplexType {

	public RequestedSecurityToken() {
		super(new QName("RequestedSecurityToken", Constants.Namespace_WS_Trust), ComplexType.CONTAINER_SEQUENCE);
		
		Element curveName = new Element(new QName("auth-ecc-dh-curvename", Constants.Namespace), SchemaUtil.TYPE_STRING);
		
		Attribute encAttr = new Attribute(new QName("EncodingType", Constants.Namespace_WS_Security), SchemaUtil.TYPE_STRING);
		encAttr.setUse(SchemaConstants.USE_OPTIONAL);
		
		ExtendedSimpleContent sc = new ExtendedSimpleContent(new QName ("sc", Constants.Namespace));
		sc.setBase(SchemaUtil.TYPE_STRING);
		sc.addAttributeElement(encAttr);
		
		Element nonce = new Element(new QName("auth-ecc-dh-nonce", Constants.Namespace), sc);
		
		Element publicKey = new Element(new QName("auth-ecc-dh-public-key", Constants.Namespace), sc);
		Element cmac = new Element(new QName("auth-ecc-dh-cmac", Constants.Namespace), sc);
		
		this.addElement(curveName);
		this.addElement(nonce);
		this.addElement(publicKey);
		this.addElement(cmac);
		
	}

}
