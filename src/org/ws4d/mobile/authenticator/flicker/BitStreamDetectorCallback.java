package org.ws4d.mobile.authenticator.flicker;

public interface BitStreamDetectorCallback {
	public void onLuminosityMeasured(short relativeLuminosity);
}
