package org.ws4d.mobile.authenticator.flicker;

public interface BitStreamDetectorKeyReadCallback {
	public void onKeyRead(Boolean[] key);
	public void onSymbolRead(Boolean symbol);
	public void onStateChanged(int state);
	public void onReset();
}
